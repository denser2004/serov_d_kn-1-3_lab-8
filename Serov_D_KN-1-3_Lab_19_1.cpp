#include <assert.h>
#include <stdio.h>
#include <iostream>
#include <windows.h>
using namespace std;

struct node
{
	int x;
	node *L;
	node *R;
};

void show_childr(node *&mt)
{
	node *cl, *cr;
	if (mt != NULL) 
	{
	
		cout << " Значення вузла = " << mt->x << " "; 
		if ((mt->L)!=NULL) 
		{
			cl= mt->L;
			cout << " Лівий нащадок = " << cl->x << " ";
		}
		if ((mt->R)!=NULL)
		{
			cr= mt->R;
			cout << " Правий нащадок = " << cr->x << " ";
		}
		cout << ";\n";
		show_childr(mt->L); 
		show_childr(mt->R); 
	}
}

int count(node *&mt)
{
	if (mt == NULL)   return 0;
	return 1+count(mt->L)+count(mt->R);
}


void del(node *&mt)
{
	if (mt != NULL) 
	{
		del(mt->L); 
		del(mt->R); 
		delete mt; 
	}
mt=NULL;
}


void add_node_balans(int x, node *&mt)
{
	if (mt == NULL)
	{
		mt = new node;
		mt->x = x;
		mt->L = NULL;
		mt->R = NULL;
	}
	else
	{
		if (count(mt->L)<count(mt->R))
		{
			if (mt->L != NULL)
			add_node_balans(x, mt->L); 
			else
				{
					mt->L = new node;
					mt->L->L = NULL;
					mt->L->R = NULL;
					mt->L->x = x;
				}
		}
		else
		{
			if (mt->R != NULL)
			add_node_balans(x, mt->R); 
			else
			{
				mt->R = new node;
				mt->R->L = NULL;
				mt->R->R = NULL;
				mt->R->x = x;
			}
		}
	}
}


void add_node(int x,node *&mt)
{
	if (mt == NULL)
	{
		mt = new node;
		mt->x = x;
		mt->L = NULL;
		mt->R = NULL;
	}
	else
	{
		if (x <= mt->x)
		{
			if (mt->L != NULL)
			add_node(x, mt->L); 
			else
			{
				mt->L = new node;
				mt->L->L = NULL;
				mt->L->R = NULL;
				mt->L->x = x;
			}
		}
		else
		{
			if (mt->R != NULL)
			add_node(x, mt->R); 
			else
			{
				mt->R = new node;
				mt->R->L = NULL;
				mt->R->R = NULL;
				mt->R->x = x;
			}
		}
	}
}


bool find_node(node *&mt, int e)
{
	int q=0;
	if (mt != NULL)
	{
		q=find_node(mt->L, e);
		if (mt->x == e)  return 1;
		q=q+find_node(mt->R, e);
		return q;
	}
	else  return 0;
}



void show_childr_fromE(node *&mt, int e)
{
	node *cl, *cr;
	
	
	
	if (mt != NULL) 
	{
		if(mt->x == e)
		{
		
		cout << " Значення вузла = " << mt->x << " "; 
		if ((mt->L)!=NULL) 
		{
			cl= mt->L;
			cout << " Лівий нащадок = " << cl->x << " ";
		}
		if ((mt->R)!=NULL)
		{
			cr= mt->R;
			cout << " Правий нащадок = " << cr->x << " ";
		}
		cout << ";\n";
		
		show_childr(mt->L); 
		show_childr(mt->R); 
		
		}
		else
		{
			show_childr_fromE(mt->L, e); 
			show_childr_fromE(mt->R, e); 
		}
	}
}




int main()
{
	const int n=15;
	int i, e, m;
	int el[n]={0,1,2,3,4,5,6,7,8,9,14,13,12,11,10};
	system("cls");
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	
	node *tree = NULL;


	cout << "\nБудуємо незбалансоване дерево: "<<"\n";	
	for(i=0;i<n;i++)
	{
		add_node(el[i], tree);	
	}
	
	
	cout << "\nКількість вузлів дерева: " <<count(tree)<<"\n";
	cout << "\nЗгенеровані вузли дерева: \n";
	show_childr(tree);
	cout << "\n";
	cout << "\nВведіть елемент, який необхідно знайти: ";
	cin >> e;
	
	if(find_node(tree, e))	cout << endl << "Такий елемент є у дереві!" << endl << endl;
	else  cout << endl << "Такого елементу немає у дереві!" << endl << endl;
	
	del(tree);
	
	cout << "\nБудуємо збалансоване дерево: "<<"\n";
	for(i=0;i<n;i++)
	{
		add_node_balans(el[i], tree);	
	}
	
	cout << "\nКількість вузлів дерева: " <<count(tree)<<"\n";
	cout << "\nЗгенеровані вузли дерева: \n";
	show_childr(tree);
	cout << "\n";
	cout << "\nВведіть елемент, який необхідно знайти: ";
	cin >> e;
	
	if(find_node(tree, e))  
	{
		cout << endl << "Такий елемент є у дереві" << endl;
		cout << " Його гiлки: " << endl;
		show_childr_fromE(tree, e);
	}
	
	else  cout << endl << "Такого елементу немає у дереві" << endl <<endl;
	

	del(tree);
	return 0;
}




































